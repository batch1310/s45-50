
import {useState} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter, Switch, Route} from 'react-router-dom';


import 'bootstrap/dist/css/bootstrap.min.css';

//for React Context
import {UserProvider} from './UserContext';

/*components*/
import AppNavbar from './components/AppNavbar';

/*pages*/
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';


export default function App() {

  /*React Context is nothing but a global state to the app. It is a way to make a particular data available to all the components no matter they are nested. Context helps you broadcast the data and changes happening to that data, to all components.*/


  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  //function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }


  return(
    <UserProvider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavbar/>
        <Container fluid className="my-4">
          <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/courses" component={Courses} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/logout" component={Logout} />

                <Route path="/*" component={ErrorPage} />
          </Switch>
        </Container>
      </BrowserRouter>
    </UserProvider>
    )
}

