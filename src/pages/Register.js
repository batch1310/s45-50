import {useState, useEffect, useContext} from 'react';
import {Container,Form,Button} from 'react-bootstrap';
import {Redirect} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(){

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [cpw, setCpw] = useState("")

	const [isLogin, setLogin] = useState(false)

	const [isDisabled, setIsDisabled] = useState(true)

	useEffect( () => {
		if( (email !== "" && password !== "" && cpw !== "") && (password === cpw) ){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}

		let data = localStorage.getItem('email');

		if(data){
			setLogin(true)	
		}
	}, [email, password, cpw])

	function Register(e) {
		e.preventDefault()

		alert("Registered Successfully!")

		setEmail("")
		setPassword("")
		setCpw("")
	}

	return(
		(isLogin) ? <Redirect to="courses" /> : 

		<Container fluid>
			<Form className="border p-3 mb-3" onSubmit={ (e) => {Register(e)} }>
				<Form.Group className="mb-3" controlId="email">
					<Form.Label>Email address</Form.Label>
					<Form.Control 
						type="email" 
						placeholder="Enter email" 
						value={email} 
						onChange={ (e) => setEmail(e.target.value) }
					/>
				</Form.Group>

			  	<Form.Group className="mb-3" controlId="password">
			    	<Form.Label>Password</Form.Label>
			    	<Form.Control 
			    		type="password" 
			    		placeholder="Password" 
			    		value={password}
			    		onChange={ (e) => setPassword(e.target.value) }
			    	/>
			  	</Form.Group>

			  	<Form.Group className="mb-3" controlId="cpw">
			    	<Form.Label>Verify Password</Form.Label>
			    	<Form.Control 
			    		type="password" 
			    		placeholder="Verify Password" 
			    		value={cpw}
			    		onChange={ (e) => setCpw(e.target.value) } 
			    	/>
			  	</Form.Group>

			  	<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
			</Form>
		</Container>
	)
}