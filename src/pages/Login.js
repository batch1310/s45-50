import {useState, useEffect, useContext} from 'react';
//useContext is used to unpack the data from the UserContext
import {Col, Row, Container, Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Login(){

	const {user, setUser} = useContext(UserContext)

	const [email, userEmail] = useState("")
	const [password, userPsw] = useState("")

	const [isDisabled, setIsDisabled] = useState(true)

	function loginUser(e){
		e.preventDefault()

		/* 	store data in the local storage
				-> localStorage.setItem("propertyName", value)
		*/
		localStorage.setItem("email", email)
		setUser({
			email: localStorage.getItem('email')
		})


		userEmail("")
		userPsw("")

		alert("Logging in...")
	}

	useEffect( () => {
		if( email !== "" && password !== "" ){
			setIsDisabled(false)
		}
	}, [email, password])

	return(

		<Container fluid>
			<Row className="justify-content-center">
				<Col xs={12} md={10}>
					<h1>Login</h1>

					<Form className=" mt-3"
						  onSubmit={(e) => loginUser(e)}
					>
					  <Form.Group className="mb-3" controlId="formBasicEmail">
					    <Form.Label>Email address</Form.Label>
					    <Form.Control 
					    	type="email" 
							placeholder="Enter email" 
							value={email} 
							onChange={ (e) => {
								userEmail(e.target.value)
							}}
					    />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicPassword">
					    <Form.Label>Password</Form.Label>
					    <Form.Control
					    	type="password" 
							placeholder="Enter email"
							value={password} 
							onChange={ (e) => {
								userPsw(e.target.value)
							}} 
					    />
					  </Form.Group>

					  <Button variant="success" type="submit" disabled={isDisabled}>Login</Button>
					</Form>
				</Col>
			</Row>
		</Container>

	)
}