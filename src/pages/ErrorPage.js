
import Banner from '../components/Banner'
import {Container, Row, Col, Jumbotron} from 'react-bootstrap';

export default function ErrorPage(){

	const data = {
		title: "404 - Page Not Found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back home"
	}

	return(
		<Banner data={data} />

		// <Container fluid>
		// 	<Row>
		// 		<Col>
		// 			<div className="jumbotron">
		// 				<h5>Zuitt Booking</h5>
		// 				<h1>Page Not found.</h1>
		// 				<p>Go back to the <a href="#">homepage</a></p>
		// 			</div>
		// 		</Col>
		// 	</Row>
		// </Container>
	)
}